/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.cartagena.training.test.constants;

/**
 * 
 */
public class CartagenaTrainingTestConstants extends GeneratedCartagenaTrainingTestConstants
{

	public static final String EXTENSIONNAME = "cartagenaTrainingtest";

}
