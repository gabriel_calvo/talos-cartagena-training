/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.cartagena.training.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.cartagena.training.core.constants.CartagenaTrainingCoreConstants;
import de.hybris.cartagena.training.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class CartagenaTrainingCoreManager extends GeneratedCartagenaTrainingCoreManager
{
	public static final CartagenaTrainingCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CartagenaTrainingCoreManager) em.getExtension(CartagenaTrainingCoreConstants.EXTENSIONNAME);
	}
}
