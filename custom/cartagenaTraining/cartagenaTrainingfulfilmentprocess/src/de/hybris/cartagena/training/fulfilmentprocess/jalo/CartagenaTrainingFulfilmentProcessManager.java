/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.cartagena.training.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.cartagena.training.fulfilmentprocess.constants.CartagenaTrainingFulfilmentProcessConstants;

public class CartagenaTrainingFulfilmentProcessManager extends GeneratedCartagenaTrainingFulfilmentProcessManager
{
	public static final CartagenaTrainingFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CartagenaTrainingFulfilmentProcessManager) em.getExtension(CartagenaTrainingFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
