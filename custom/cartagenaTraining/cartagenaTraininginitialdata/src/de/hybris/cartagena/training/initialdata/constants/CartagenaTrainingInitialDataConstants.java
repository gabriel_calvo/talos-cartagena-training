/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.cartagena.training.initialdata.constants;

/**
 * Global class for all CartagenaTrainingInitialData constants.
 */
public final class CartagenaTrainingInitialDataConstants extends GeneratedCartagenaTrainingInitialDataConstants
{
	public static final String EXTENSIONNAME = "cartagenaTraininginitialdata";

	private CartagenaTrainingInitialDataConstants()
	{
		//empty
	}
}
