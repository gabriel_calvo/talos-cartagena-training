/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.cartagena.training.facades.constants;

/**
 * Global class for all CartagenaTrainingFacades constants.
 */
public class CartagenaTrainingFacadesConstants extends GeneratedCartagenaTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "cartagenaTrainingfacades";

	private CartagenaTrainingFacadesConstants()
	{
		//empty
	}
}
